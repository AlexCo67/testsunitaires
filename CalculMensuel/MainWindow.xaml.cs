﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Text.RegularExpressions;
using System.Windows.Shapes;
using Bibliotheque;

namespace CalculMensuel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }


        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            string durationInYearsString = LoanDurationYearsComboBox.Text;
            if (durationInYearsString != null)
            {
                int durationInYears = Int32.Parse(durationInYearsString)*12;
                LoanDurationMonthsTextBlock.Text= durationInYears.ToString();
            }
        }

        private void Validate(object sender, RoutedEventArgs e)
        {
            bool sportive = (SportCheckBox.IsChecked == true);
            bool smoker = (SmokerCheckBox.IsChecked == true);
            bool heartDisease = (HeartCheckBox.IsChecked == true);
            bool iTEngineer = (ITCheckBox.IsChecked == true);
            bool jetPilot = (JetCheckBox.IsChecked == true);

            Client client = new Client(sportive, smoker, heartDisease, iTEngineer, jetPilot);
            double insuranceRate = InsuranceRatioCalculator.InsuranceRate(client);
            Insurance insurance = new Insurance(insuranceRate);
            int ammount;
            string rateType;
            int duration;
            
            if (!LoanAmmountCheck())
            {
                return;
            }
            ammount = Int32.Parse(LoanAmmountTextBox.Text);

            if (!LoanDurationCheck())
            {
                return;
            }
            duration = Int32.Parse(LoanDurationYearsComboBox.Text);

            if (!LoanRateCheck())
            {
                return;
            }
            rateType = LoanRateComboBox.Text;


            double interestRate = InterestRateCalculator.InterestRateValue(duration, rateType);

            Loan loan = new Loan(ammount, duration, interestRate);

            ValuesSummaryVisualUpdate(loan, insurance);
            
        }


        private bool LoanAmmountCheck()
        {
            int ammount;
            try
            {
                ammount = Int32.Parse(LoanAmmountTextBox.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Veuillez remplir la valeur de prêt");
                return false;
            }
            if (ammount < 50000)
            {
                MessageBox.Show("Veuillez mettre une valeur supérieure à 50000€");
                return false;
            }
            return true;
        }

        private bool LoanDurationCheck()
        {
            try
            {
                int duration = Int32.Parse(LoanDurationYearsComboBox.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Veuillez choisir une durée de prêt");
                return false;
            }
            return true;
        }

        private bool LoanRateCheck()
        {
            string rateType;
            try
            {
               rateType = LoanRateComboBox.Text;
            }
            catch (Exception)
            {
                MessageBox.Show("Veuillez choisir un type de prêt");
                return false;
            }
            if (rateType == "")
            {
                MessageBox.Show("Veuillez choisir un type de prêt");
                return false;
            }
            return true;
        }

        private void ValuesSummaryVisualUpdate(Loan loan, Insurance insurance)
        {
            TotalPriceTextBlock.Text = Math.Round(TotalCalculator.MonthlyAmmountWithInsurance(loan, insurance)).ToString() + " €";
            ContributionTextBlock.Text = Math.Round(TotalCalculator.MonthlyInsurance(loan, insurance)).ToString() + " €";
            FinalInterestRateTextBlock.Text = (Math.Round(100 * loan.interestRate, 2)).ToString() + " %";
            InsuranceTotalTextBlock.Text = Math.Round(TotalCalculator.TotalInsuranceAmmount(loan, insurance)).ToString() + " €";
            FinalLoanDurationTextBlock.Text = loan.duration.ToString() + " ans";
            AmmountRefundedAfter10YearsTextBlock.Text = Math.Round(100 * TotalCalculator.PayedCapitalAt10Years(loan, insurance)).ToString() + " %";
            MontlhyAmmountWithoutInsuranceTextBlock.Text = Math.Round(TotalCalculator.MonthlyAmmountWithoutInsurance(loan)).ToString() + " €";
            InsuranceRateTextBlock.Text = (Math.Round(100 * insurance.insuranceRate, 2)).ToString() + " %";
            InterestsTotalTextBlock.Text = Math.Round(TotalCalculator.TotalInterestsAmmount(loan, insurance)).ToString() + " €";

        }


    }
}
