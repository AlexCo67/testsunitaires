﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bibliotheque;

namespace TestCalculsPrets.CalculatorTests
{
    public class ClientTestData
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] {0.008f, new Client(true, true, true,true,true) };
                yield return new object[] { 0.004f, new Client(true, true, false, false, false) };
                yield return new object[] { 0.007f, new Client(false, false, true, true, true) };
                yield return new object[] { 0.003f, new Client(false, false, false, false, false) };
            }
        }
    }
}
