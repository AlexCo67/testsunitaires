﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bibliotheque;

namespace TestCalculsPrets.CalculatorTests
{
    internal class LoanAndInsuranceTestData
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] {new Loan(50000, 10, 0.0067f), new Insurance(0.003f) };
            }
        }
    }
}
