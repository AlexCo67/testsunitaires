﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bibliotheque;

namespace TestCalculsPrets.CalculatorTests
{
    public  class LoanTestData
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] {430, new Loan(50000,10,0.0067f)};
            }
        }
    }
}

