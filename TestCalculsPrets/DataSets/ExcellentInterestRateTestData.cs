﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCalculsPrets.CalculatorTests
{
    public class ExcellentInterestRateTestData
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] { 10, "Excellent Taux" };
            }
        }
    }
}
