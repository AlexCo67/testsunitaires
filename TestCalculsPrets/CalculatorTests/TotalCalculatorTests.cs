﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Bibliotheque;

namespace TestCalculsPrets.CalculatorTests
{
    public class TotalCalculatorTests
    {
        [Trait("Category", "TotalCalculator")]
        [Theory]
        [MemberData(nameof(LoanTestData.TestData), MemberType = typeof(LoanTestData))]
        public void MonthlyAmmountWithoutInsuranceTest(float expectedValue, Loan loan)
        {
            Assert.Equal(expectedValue, TotalCalculator.MonthlyAmmountWithoutInsurance(loan), 0);
        }

        [Trait("Category", "TotalCalculator")]
        [Theory]
        [MemberData(nameof(LoanAndInsuranceTestData.TestData), MemberType = typeof(LoanAndInsuranceTestData))]

        public void MonthlyInsuranceTest(Loan loan, Insurance insurance)
        {
            Assert.Equal(13, TotalCalculator.MonthlyInsurance(loan, insurance), 0);
        }

        [Trait("Category", "TotalCalculator")]
        [Theory]
        [MemberData(nameof(LoanAndInsuranceTestData.TestData), MemberType = typeof(LoanAndInsuranceTestData))]

        public void MonthlyAmmountWithInsuranceTest(Loan loan, Insurance insurance)
        {
            Assert.Equal(443, TotalCalculator.MonthlyAmmountWithInsurance(loan, insurance), 0);
        }


        [Trait("Category", "TotalCalculator")]
        [Theory]
        [MemberData(nameof(LoanAndInsuranceTestData.TestData), MemberType = typeof(LoanAndInsuranceTestData))]

        public void TotalInterestsAmmountTest(Loan loan, Insurance insurance)
        {
            Assert.Equal(1708, TotalCalculator.TotalInterestsAmmount(loan, insurance), 0);
        }

        [Trait("Category", "TotalCalculator")]
        [Theory]
        [MemberData(nameof(LoanAndInsuranceTestData.TestData), MemberType = typeof(LoanAndInsuranceTestData))]

        public void TotalInsuranceAmmountTest(Loan loan, Insurance insurance)
        {
            Assert.Equal(1500, TotalCalculator.TotalInsuranceAmmount(loan, insurance), 0);
        }

        [Trait("Category", "TotalCalculator")]
        [Theory]
        [MemberData(nameof(LoanAndInsuranceTestData.TestData), MemberType = typeof(LoanAndInsuranceTestData))]

        public void PayedCapitalAt10YearsTest(Loan loan, Insurance insurance)
        {
            Assert.Equal(1, TotalCalculator.PayedCapitalAt10Years(loan, insurance), 0);
        }


    }
}
