﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Bibliotheque;

namespace TestCalculsPrets.CalculatorTests
{
    public class InsuranceRatioCalculatorTests
    {
        [Trait("Category", "Insurance")]
        [Theory]
        [MemberData(nameof(ClientTestData.TestData), MemberType = typeof(ClientTestData))]
        public void TestInsuranceRateCalculation(float expectedValue, Client client)
        {
            Assert.Equal(expectedValue, InsuranceRatioCalculator.InsuranceRate(client), 0);
        }
    }
}

