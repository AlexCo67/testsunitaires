﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Bibliotheque;

namespace TestCalculsPrets.CalculatorTests
{
    public class InterestRateCalculatorTests
    {
        [Trait("Category", "Insurance")]
        [Theory]
        [MemberData(nameof(GoodInterestRateTestData.TestData), MemberType = typeof(GoodInterestRateTestData))]
        public void GoodRateValueTest(int duration, string rateType)
        {
            Assert.Equal(0.0067f, InterestRateCalculator.GoodRateValue(duration), 0);
        }

        [Trait("Category", "Insurance")]
        [Theory]
        [MemberData(nameof(VeryGoodInterestRateTestData.TestData), MemberType = typeof(VeryGoodInterestRateTestData))]
        public void VeryGoodRateValueTest(int duration, string rateType)
        {
            Assert.Equal(0.0055f, InterestRateCalculator.VeryGoodRateValue(duration), 0);
        }

        [Trait("Category", "Insurance")]
        [Theory]
        [MemberData(nameof(ExcellentInterestRateTestData.TestData), MemberType = typeof(ExcellentInterestRateTestData))]
        public void ExcellentRateValueTest(int duration, string rateType)
        {
            Assert.Equal(0.0045f, InterestRateCalculator.ExcellentRateValue(duration), 0);
        }
    }
}
