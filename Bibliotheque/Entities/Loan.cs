﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public class Loan
    {
        public Loan(int ammount, int duration, double interestRate)
        {
            this.ammount = ammount;
            this.duration = duration;
            this.interestRate = interestRate;
            
        }

        public int ammount { get; }
        public int duration { get; }
        public double interestRate { get; }

    }
}
