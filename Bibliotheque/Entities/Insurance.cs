﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public class Insurance
    {
        public Insurance(double insuranceRate)
        {
            this.insuranceRate = insuranceRate;
        }

        public double insuranceRate { get; }


    }
}
