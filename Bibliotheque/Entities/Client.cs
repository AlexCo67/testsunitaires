﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public class Client
    {
        public Client(bool sportive, bool smoker, bool heartDisease, bool iTEngineer, bool jetPilot)
        {
            this.sportive = sportive;
            this.smoker = smoker;
            this.heartDisease = heartDisease;
            this.iTEngineer = iTEngineer;
            this.jetPilot = jetPilot;
        }

        public bool sportive { get; }
        public bool smoker { get; }
        public bool heartDisease { get; }
        public bool iTEngineer { get; }
        public bool jetPilot { get; }

    }
}
