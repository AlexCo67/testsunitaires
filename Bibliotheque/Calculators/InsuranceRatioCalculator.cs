﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public static class InsuranceRatioCalculator
    {
        public static double InsuranceRate(Client client)
        {
            float ratio = InsuranceRatioValues.BASE_RATIO;

            if (client.sportive)
            {
                ratio += InsuranceRatioValues.RATIO_SPORTIVE;
            }
            if (client.smoker)
            {
                ratio += InsuranceRatioValues.RATIO_SMOKER;
            }
            if (client.heartDisease)
            {
                ratio += InsuranceRatioValues.RATIO_HEART_DISEASE;
            }
            if (client.iTEngineer)
            {
                ratio += InsuranceRatioValues.RATIO_ITENGINEER;
            }
            if (client.jetPilot)
            {
                ratio += InsuranceRatioValues.RATIO_JET_PILOT;
            }

            return ratio/100;
        }
    }
}

