﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public class InterestRateCalculator
    {
        public static double InterestRateValue(int duration, string rateType)
        {
            double interestRateValue=1;
            if (rateType=="Bon Taux")
            {
                interestRateValue = GoodRateValue(duration);
            }
            else if (rateType=="Très Bon Taux")
            {
                interestRateValue = VeryGoodRateValue(duration);
            }
            else if (rateType=="Excellent Taux")
            {
                interestRateValue = ExcellentRateValue(duration);
            }
                return interestRateValue;
        }

        public static double GoodRateValue(int duration)
        {
            switch (duration)
            {
                case 7: return 0.62 / 100;
                case 10: return 0.67 / 100;
                case 15: return 0.85 / 100;
                case 20: return 1.04 / 100;
                case 25: return 1.27 / 100;
                default: return 1 / 100;
            }
        }

        public static double VeryGoodRateValue(int duration)
        {
            switch (duration)
            {
                case 7: return 0.43 / 100;
                case 10: return 0.55 / 100;
                case 15: return 0.73 / 100;
                case 20: return 0.91 / 100;
                case 25: return 1.15 / 100;
                default: return 1 / 100;
            }
        }

        public static double ExcellentRateValue(int duration)
        {
            switch (duration)
            {
                case 7: return 0.35 / 100;
                case 10: return 0.45 / 100;
                case 15: return 0.58 / 100;
                case 20: return 0.73 / 100;
                case 25: return 0.89 / 100;
                default: return 1 / 100;
            }
        }
    }
}
