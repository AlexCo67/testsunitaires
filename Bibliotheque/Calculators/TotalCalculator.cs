﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public class TotalCalculator
    {

        public static int MONTH_PER_YEAR = 12;


        public static double MonthlyAmmountWithInsurance(Loan loan, Insurance insurance)
        {
            return (MonthlyAmmountWithoutInsurance(loan) + MonthlyInsurance(loan, insurance));
        }


        public static double MonthlyAmmountWithoutInsurance(Loan loan)
        {
            return (loan.ammount * loan.interestRate / MONTH_PER_YEAR) / (1 - Math.Pow(1 + loan.interestRate / 12, -loan.duration*MONTH_PER_YEAR));
        }

        public static double MonthlyInsurance(Loan loan, Insurance insurance)
        {
            return loan.ammount * insurance.insuranceRate / MONTH_PER_YEAR;
        }

        public static double TotalAmmount(Loan loan, Insurance insurance)
        {
            return MonthlyAmmountWithInsurance(loan, insurance) * loan.duration * MONTH_PER_YEAR;
        }

        public static double TotalInterestsAmmount(Loan loan, Insurance insurance)
        {
            return TotalAmmount(loan, insurance) - TotalInsuranceAmmount(loan, insurance)-loan.ammount;
        }

        public static double TotalInsuranceAmmount(Loan loan, Insurance insurance)
        {
            return MonthlyInsurance(loan, insurance) * loan.duration * MONTH_PER_YEAR;
        }

        public static double PayedCapitalAt10Years(Loan loan, Insurance insurance)
        {
            double payedCapital;
            if (loan.duration == 7)
            {
                payedCapital = 1;
            }
            else
            {
                payedCapital = MonthlyAmmountWithInsurance(loan, insurance) * 10 * MONTH_PER_YEAR / TotalAmmount(loan, insurance);
            }
            return payedCapital;
        }
    }
}
