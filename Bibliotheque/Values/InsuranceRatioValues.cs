﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bibliotheque
{
    public static class InsuranceRatioValues
    {
        public static readonly float BASE_RATIO = 0.3f;
        public static readonly float RATIO_SPORTIVE = -0.05f;
        public static readonly float RATIO_SMOKER = 0.15f;
        public static readonly float RATIO_HEART_DISEASE = 0.3f;
        public static readonly float RATIO_ITENGINEER = -0.05f;
        public static readonly float RATIO_JET_PILOT = 0.15f;
    }

}
